import styles from '../styles/favorite.module.css'
import Navbar from '../components/Navbar'
import Card from '../components/Card'
import Footer from '../components/Footer'
import { useEffect, useState } from "react";

export default function favorite() {

    const[restoAsync, setrestoAsync] = useState ([])
    const[restoPromise, setrestoPromise] = useState ([])
  
    const restoFetchPromise = () => {
      const response = fetch('./api/restaurants')
      response
      .then(res => res.json())
      .then((data => {
        setrestoPromise(data)
        console.log(data)
      }))
      .catch(e => {console.log(e)});
  
    };
   
    async function restoFetchAsync() {
      try {
        const res = await fetch('./api/restaurants');
        const data = await res.json();
        setrestoAsync(data);
        console.log(data);
      } catch (e) {
        console.error(e);
      }
    }

      
    useEffect(() => {
        restoFetchPromise();
      },[])

    return (
      <div className={styles.container}>
        <Navbar/>
        <div className={styles.main}>
            <h1 className={styles.title}>Find Something Nice Here</h1>
        </div>
        <div className={styles.row}>
            {restoPromise.map((restaurants)=>
            <div className={styles.col} key={restaurants.id}>
                <Card 
                  loc={restaurants.location}
                  image={restaurants.image}
                  rate={restaurants.rate}
                  title={restaurants.name}
                  desc={restaurants.description}
                />
            </div>
            )} 
          </div>
        <div className={styles.main}></div>
        <Footer/>   
      </div>
       
    )
  }
  